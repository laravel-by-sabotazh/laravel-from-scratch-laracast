## Tutorial

From [here](https://laracasts.com/series/laravel-8-from-scratch).

## Software

Laravel v9.43.0

PHP v8.2.0

## License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
