<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
//        User::truncate();
//        Post::truncate();
//        Category::truncate();

        $user = User::factory()->create([
            'name' => 'John Doe'
        ]);

        Post::factory(5)->create([
            'user_id' => $user->id
        ]);

//        $user = User::factory()->create();
//
//        $work = Category::create([
//            'name' => 'Work',
//            'slug' => 'work'
//        ]);
//
//        $home = Category::create([
//            'name' => 'Home',
//            'slug' => 'home'
//        ]);
//
//        Post::create([
//            'user_id' => $user->id,
//            'category_id' => $work->id,
//            'title' => 'My work post',
//            'slug' => 'my-first-post',
//            'excerpt' => 'Aliquam aut consequuntur, dignissimos dolore facilis libero magni molestias.',
//            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut consequuntur, dignissimos dolore facilis libero magni molestias, nobis nostrum perspiciatis provident quae ratione, voluptatibus. Atque error et recusandae reprehenderit tempore!<p>'
//        ]);
//
//        Post::create([
//            'user_id' => $user->id,
//            'category_id' => $home->id,
//            'title' => 'My home post',
//            'slug' => 'my-second-post',
//            'excerpt' => 'Beatae consequuntur dolor fuga iusto modi quidem quo ratione repellat saepe.',
//            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae consequuntur dolor fuga iusto modi quidem quo ratione repellat saepe. Ab eius quaerat quia reprehenderit! Asperiores dicta esse natus nostrum voluptates.<p>'
//        ]);
    }
}
